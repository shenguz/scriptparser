﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;

namespace ShenGu.Script
{
    public static class ScriptUtils
    {
        private readonly static Type NullableType;
        private static Dictionary<Type, ScriptTypeMembers> dicMembers = new Dictionary<Type, ScriptTypeMembers>();
        private static ReaderWriterLock dicLock = new ReaderWriterLock();
        private static readonly Type ObjectType = typeof(object);
        private static ScriptObjectConvertHandler objectConverter;

        static ScriptUtils()
        {
            NullableType = typeof(int?).GetGenericTypeDefinition();
            RegisterType(typeof(DataTableProxy));
            RegisterType(typeof(DataRowProxy));
        }

        #region 私有方法

        private static void AppendString(StringBuilder sb, string value)
        {
            sb.Append('"');
            int length = value.Length;
            if (length > 0)
            {
                int last = 0;
                for (int i = 0; i < length; i++)
                {
                    char ch = value[i];
                    if (ch == '\\' || ch == '"')
                    {
                        if (i > last) sb.Append(value, last, i - last);
                        sb.Append('\\');
                        sb.Append(ch);
                        last = i + 1;
                    }
                }
                if (last == 0) sb.Append(value);
                else if (last < length) sb.Append(value, last, length - last);
            }
            sb.Append('"');
        }
        
        private static void AppendObject(StringBuilder sb, IScriptObject value, ScriptContext context, bool quoteField)
        {
            if (value is ScriptString) AppendString(sb, ((ScriptString)value).Value);
            else if (value is ScriptUndefined || value is ScriptNull || value is ScriptBoolean || value is ScriptNumber)
                sb.Append(value.ToValueString(context));
            else
            {
                IScriptArray array = value as IScriptArray;
                if (array != null && array.IsArray)
                {
                    int arrayLength = array.ArrayLength;
                    sb.Append('[');
                    for (int i = 0; i < arrayLength; i++)
                    {
                        if (i > 0) sb.Append(',');
                        AppendObject(sb, array.GetElementValue(context, i), context, quoteField);
                    }
                    sb.Append(']');
                }
                else
                {
                    sb.Append('{');
                    bool isFirst = true;
                    IEnumerator en = value.GetEnumerator(context, true);
                    while (en.MoveNext())
                    {
                        object obj = en.Current;
                        string key;
                        if (obj is IScriptObject)
                            key = ScriptUtils.ToValue(context, (IScriptObject)obj, typeof(string)) as string;
                        else
                            key = obj != null ? obj.ToString() : null;

                        if (key != null)
                        {
                            if (isFirst) isFirst = false;
                            else sb.Append(',');
                            if (quoteField) sb.Append('"');
                            sb.Append(key);
                            if (quoteField) sb.Append('"');
                            sb.Append(':');
                            IScriptObject v = value.GetValue(context, key);
                            AppendObject(sb, v, context, quoteField);
                        }
                    }
                    sb.Append('}');
                }
            }
        }
        
        private static object ToDefaultValue(Type toType)
        {
            TypeCode tcode = Type.GetTypeCode(toType);
            switch (tcode)
            {
                case TypeCode.DBNull: return DBNull.Value;
                case TypeCode.Char: return '\0';
                case TypeCode.Byte: return (byte)0;
                case TypeCode.Int16: return (short)0;
                case TypeCode.UInt16: return (ushort)0;
                case TypeCode.Int32: return 0;
                case TypeCode.UInt32: return (uint)0;
                case TypeCode.Int64: return 0L;
                case TypeCode.UInt64: return (ulong)0;
                case TypeCode.Single: return 0f;
                case TypeCode.Double: return 0d;
                case TypeCode.Decimal: return 0m;
                case TypeCode.DateTime: return DateTime.MinValue;
                default: return null;
            }
        }

        private static int ReadIndex(string template, int index)
        {
            if (index + 2 < template.Length)
            {
                char ch = template[index];
                if (ch >= '0' && ch <= '9')
                {
                    char ch2 = template[index + 1];
                    if (ch > '0' && ch2 >= '0' && ch2 <= '9')
                    {
                        if (index + 3 < template.Length && template[index + 2] == '}')
                            return (ch - '0') * 10 + (ch2 - '0');
                    }
                    else if (ch2 == '}')
                        return ch - '0';
                }
            }
            return -1;
        }

        #endregion

        #region 内部方法

        internal static ScriptTypeMembers InternalGetMembers(Type type)
        {
            ScriptTypeMembers result;
            if (type == null || type == ObjectType) result = null;
            else
            {
                bool exist;
                dicLock.AcquireReaderLock(-1);
                try
                {
                    exist = dicMembers.TryGetValue(type, out result);
                }
                finally
                {
                    dicLock.ReleaseReaderLock();
                }
                if (!exist && ScriptTypeMembers.IsScriptType(type))
                {
                    ScriptTypeMembers mem = ScriptTypeMembers.Load(type, ObjectMemberFlags.None);
                    dicLock.AcquireWriterLock(-1);
                    try
                    {
                        if (!dicMembers.TryGetValue(type, out result))
                            dicMembers[type] = result = mem;
                    }
                    finally
                    {
                        dicLock.ReleaseWriterLock();
                    }
                }
            }
            return result;
        }

        #endregion

        #region 公共方法/属性

        /// <summary>注册类型，主要是缓存类型对应的元素。</summary>
        public static void RegisterType(Type type)
        {
            RegisterType(type, ObjectMemberFlags.None);
        }

        /// <summary>注册类型，主要是缓存类型对应的元素。</summary>
        /// <remarks>
        /// 正常情况，当脚本中使用到.net的类型时，会自动将该类型注册到当前的ScriptContext中，不需要先注册。但是，有两种情况，可以预先注册：
        /// 1. 存在类型代理，如果没有注册，在脚本解析到相应对象时，不会使用代理去处理。比如：DataTable和DataRow。
        /// 2. 比较频繁使用的类型，并且类型被标识成脚本方法或属性的成员比较多时，可以先注册，避免每次读取到的时候，都要再解析一遍。
        /// </remarks>
        public static void RegisterType(Type type, ObjectMemberFlags defaultMemberFlags)
        {
            ScriptTypeMembers members = ScriptTypeMembers.Load(type, defaultMemberFlags);
            dicLock.AcquireWriterLock(-1);
            try
            {
                dicMembers[members.Type] = members;
            }
            finally
            {
                dicLock.ReleaseWriterLock();
            }
        }

        /// <summary>注册类型，主要是缓存类型对应的元素</summary>
        public static void RegisterTypes(params Type[] types)
        {
            RegisterTypes(ObjectMemberFlags.None, types);
        }

        /// <summary>注册类型，主要是缓存类型对应的元素</summary>
        public static void RegisterTypes(ObjectMemberFlags defaultMemberFlags, params Type[] types)
        {
            int count = types != null ? types.Length : 0;
            if (count > 0)
            {
                ScriptTypeMembers[] typeMembers = new ScriptTypeMembers[count];
                for(int i = 0; i < count; i++)
                {
                    Type t = types[i];
                    if (t != null)
                        typeMembers[i] = ScriptTypeMembers.Load(t, defaultMemberFlags);
                }
                dicLock.AcquireWriterLock(-1);
                try
                {
                    for(int i = 0; i < count; i++)
                    {
                        ScriptTypeMembers m = typeMembers[i];
                        if (m != null)
                            dicMembers[m.Type] = m;
                    }
                }
                finally
                {
                    dicLock.ReleaseWriterLock();
                }
            }
        }

        /// <summary>注册类型，并指定其可以在脚本中使用的类型成员和实例成员</summary>
        public static void RegisterType(Type type, IScriptMemberList typeMembers, IScriptMemberList instanceMembers)
        {
            ScriptTypeMembers members = new ScriptTypeMembers(type, null, typeMembers, instanceMembers);
            dicLock.AcquireWriterLock(-1);
            try
            {
                dicMembers[type] = members;
            }
            finally
            {
                dicLock.ReleaseWriterLock();
            }
        }

        public static IScriptMemberList GetInstanceMembers(ScriptContext context, Type type)
        {
            if (context != null) return context.GetInstanceMembers(type);
            ScriptTypeMembers mem = InternalGetMembers(type);
            if (mem == null) ScriptExecuteException.Throw(context, string.Format("找不到类型为“{0}”的实例成员列表。", type));
            return mem.InstanceMembers;
        }

        public static IScriptMemberList GetTypeMembers(ScriptContext context, Type type)
        {
            if (context != null) return context.GetTypeMembers(type);
            ScriptTypeMembers mem = InternalGetMembers(type);
            if (mem == null) ScriptExecuteException.Throw(context, string.Format("找不到类型为“{0}”的实例成员列表。", type));
            return mem.TypeMembers;
        }

        /// <summary>将脚本对象转成字符串</summary>
        public static string Stringify(ScriptContext context, IScriptObject value, bool quoteField)
        {
            StringBuilder sb = new StringBuilder();
            AppendObject(sb, value, context, quoteField);
            return sb.ToString();
        }

        /// <summary>将脚本对象转成字符串</summary>
        public static string Stringify(ScriptContext context, IScriptObject value)
        {
            return Stringify(context, value, false);
        }

        /// <summary>将.net的对象，转成可以在脚本中运行的对象</summary>
        public static IScriptObject ToScriptObject(ScriptContext context, object value)
        {
            if (value == null) return ScriptNull.Value;
            if (value is IScriptObject) return (IScriptObject)value;
            if (value is IScriptObject[]) return new ScriptArray((IScriptObject[])value);
            if (value is Type) return new ScriptType(context, (Type)value);
            if (value is Delegate) return new ScriptNativeDelegate((Delegate)value);
            Type type = value.GetType();
            if (type.IsArray)
                return new ScriptNativeArray((Array)value);
            TypeCode tcode = Type.GetTypeCode(type);
            switch (tcode)
            {
                case TypeCode.DBNull: return ScriptNull.Value;
                case TypeCode.Boolean: return ScriptBoolean.Create((bool)value);
                case TypeCode.String: return ScriptString.Create((string)value);
                case TypeCode.Char: return ScriptString.Create(new string((char)value, 1));
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    return ScriptNumber.Create(Convert.ToInt64(value));
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return ScriptNumber.Create(Convert.ToDecimal(value));
                case TypeCode.DateTime:
                    return ScriptDate.Create((DateTime)value);
            }
            if (context == null) context = ScriptContext.Current;
            IScriptMemberList members = GetInstanceMembers(context, type);
            IScriptMemberSupportProxy m = members as IScriptMemberSupportProxy;
            if (m != null && m.ProxyType != null)
            {
                if (type == m.ProxyType)
                {
                    IScriptProxy proxy = (IScriptProxy)value;
                    proxy.RealInstance = Activator.CreateInstance(m.RealType);
                }
                else
                {
                    IScriptProxy proxy = (IScriptProxy)Activator.CreateInstance(m.ProxyType);
                    proxy.RealInstance = value;
                    value = proxy;
                }
            }
            if (value is IScriptObject) return (IScriptObject)value;
            return ScriptNativeObject.CreateNativeObject(context, value, members);
        }

        private static object CheckEnumValue(object value, Type type)
        {
            if (type.IsEnum)
                value = Enum.ToObject(type, value);
            return value;
        }

        /// <summary>将脚本中运行的对象，转成具体的.net对象</summary>
        public static object ToValue(ScriptContext context, IScriptObject obj, Type toType)
        {
            bool isObject = toType == typeof(object);
            if (!isObject && toType.IsInstanceOfType(obj)) return obj;
            if (toType.IsGenericType && toType.GetGenericTypeDefinition() == NullableType)
            {
                if (IsNull(obj)) return null;
                else
                {
                    return ToValue(context, obj, toType.GetGenericArguments()[0]);
                }
            }
            if (IsNull(obj)) return ToDefaultValue(toType);
            if (context == null) context = ScriptContext.Current;
            if (isObject) return obj.ToValue(context);
            if (typeof(string).IsAssignableFrom(toType)) return obj.ToValueString(context);
            if (toType.IsArray)
            {
                IScriptArray array = obj as IScriptArray;
                if (array != null && array.IsArray)
                {
                    Type elemType = toType.GetElementType();
                    int length = array.ArrayLength;
                    Array to = Array.CreateInstance(toType.GetElementType(), length);
                    for (int i = 0; i <length; i++)
                    {
                        IScriptObject scriptItem = array.GetElementValue(context, i);
                        object item = ToValue(context, scriptItem, elemType);
                        to.SetValue(item, i);
                    }
                    return to;
                }
            }
            TypeCode tcode = Type.GetTypeCode(toType);
            if (obj is ScriptObject && tcode == TypeCode.Object)
                return ConvertObjectByMembers(context, (ScriptObject)obj, toType);
            object result = obj.ToValue(context);
            if (toType != null)
            {
                if (result == null) result = ToDefaultValue(toType);
                else
                {
                    switch (tcode)
                    {
                        case TypeCode.DBNull: return DBNull.Value;
                        case TypeCode.Boolean: return Convert.ToBoolean(result);
                        case TypeCode.Char: return Convert.ToChar(result);
                        case TypeCode.Byte:
                            {
                                byte r = Convert.ToByte(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.SByte:
                            {
                                sbyte r = Convert.ToSByte(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.Int16:
                            {
                                short r = Convert.ToInt16(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.UInt16:
                            {
                                ushort r = Convert.ToUInt16(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.Int32:
                            {
                                int r = Convert.ToInt32(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.UInt32:
                            {
                                uint r = Convert.ToUInt32(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.Int64:
                            {
                                long r = Convert.ToInt64(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.UInt64:
                            {
                                ulong r = Convert.ToUInt64(result);
                                return toType.IsEnum ? Enum.ToObject(toType, r) : r;
                            }
                        case TypeCode.Single: return Convert.ToSingle(result);
                        case TypeCode.Double: return Convert.ToDouble(result);
                        case TypeCode.Decimal: return Convert.ToDecimal(result);
                        case TypeCode.DateTime: return Convert.ToDateTime(result);
                    }
                    if (!toType.IsAssignableFrom(result.GetType()))
                        ScriptExecuteException.Throw(context, string.Format("无法将类型“{0}”强制转化成类型“{1}”", result.GetType(), toType));
                }
            }
            return result;
        }

        /// <summary>脚本对象是否为null。</summary>
        public static bool IsNull(IScriptObject obj)
        {
            return obj == null || obj is ScriptNull || obj is ScriptUndefined;
        }

        /// <summary>脚本对象是否可以判断为true</summary>
        public static bool IsTrue(IScriptObject obj)
        {
            ScriptObjectBase scriptValue = obj as ScriptObjectBase;
            return scriptValue == null || scriptValue.BooleanValue;
        }

        /// <summary>类似于<c>string.Format</c></summary>
        public static string FormatString(string template, params string[] argus)
        {
            StringBuilder sb = null;
            if (argus != null && argus.Length > 0)
            {
                int index = 0, lastIndex = 0;
                do
                {
                    index = template.IndexOf('{', index);
                    if (index >= 0)
                    {
                        int value = ReadIndex(template, index + 1);
                        if (value >= 0)
                        {
                            if (value >= argus.Length)
                                throw new ArgumentOutOfRangeException("argus", string.Format("格式化错误：索引“{0}”超出了参数数组的长度。", value));
                            if (sb == null)
                            {
                                int length = template.Length;
                                foreach(string str in argus)
                                    if (str != null) length += str.Length;
                                sb = new StringBuilder(length);
                            }
                            if (lastIndex < index)
                                sb.Append(template, lastIndex, index - lastIndex);
                            sb.Append(argus[value]);
                            lastIndex = index + (value < 10 ? 3 : 4);
                            index = lastIndex;
                        }
                        else
                            index++;
                    }
                    else
                    {
                        if (sb != null)
                            sb.Append(template, lastIndex, template.Length - lastIndex);
                        break;
                    }
                } while (true);
            }
            return sb == null ? template : sb.ToString();
        }

        /// <summary>执行一段JavaScript语法，并返回最后一道语法的结果（相当于在.net环境执行一次JavaScript的“eval”方法）</summary>
        /// <param name="context">脚本上下文</param>
        /// <param name="script">JavaScript脚本（脚本中不要有“return”语法）</param>
        /// <returns>返回最后一道语法的执行结果</returns>
        public static IScriptObject Eval(ScriptContext context, string script)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            return context.Eval(script);
        }

        /// <summary>执行脚本</summary>
        /// <param name="context">脚本上下文，可以添加一些预设的变量，供脚本使用</param>
        /// <param name="script">JavaScript脚本</param>
        /// <returns>脚本执行后的返回值（只有执行了“return ***;”语法时才有返回值，否则，返回<c>null</c>）</returns>
        public static object ExecuteScript(ScriptContext context, string script)
        {
            ScriptParser parser = ScriptParser.Parse(script);
            parser.Execute(context);
            return context.Result;
        }

        /// <summary>解析模板</summary>
        /// <param name="context">脚本上下文，可以添加一些预设的变量，供脚本使用</param>
        /// <param name="tempate">模板内容</param>
        /// <returns>返回模板解析后的结果</returns>
        public static string ExecuteTemplate(ScriptContext context, string tempate)
        {
            TemplateParser parser = new TemplateParser();
            string script = parser.Parse(tempate, false);
            StringTemplateExecutor executor = new StringTemplateExecutor(tempate);
            parser.Execute(script, context, executor);
            return executor.ToString();
        }

        /// <summary>指定全局的<c>ScriptObject</c>转换方法。如果没有设置，在对<c>ScriptObject</c>对象调用<c>ToValue</c>时，返回对象本身，不做转化</summary>
        public static ScriptObjectConvertHandler ObjectConverter
        {
            get { return objectConverter; }
            set { objectConverter = value; }
        }

        private static object ConvertObjectByMembers(ScriptContext context, ScriptObject obj, Type toType)
        {
            object instance = Activator.CreateInstance(toType);
            IScriptObject scriptInstance = ToScriptObject(context, instance);
            foreach (KeyValuePair<string, IScriptObject> kv in obj)
            {
                scriptInstance.SetValue(context, kv.Key, kv.Value);
            }
            return instance;
        }

        #endregion
    }

    public delegate object ScriptObjectConvertHandler(ScriptContext context, ScriptObject instance);

    /// <summary>导入脚本来源管理</summary>
    public interface IImportSourceManager
    {
        /// <summary>原始来源</summary>
        IImportSource GetSource(ScriptContext context, string path);
    }

    /// <summary>导入脚本来源</summary>
    public interface IImportSource
    {
        /// <summary>路径主键，用于唯一标识一个路径。</summary>
        string PathKey { get; }

        /// <summary>导入脚本的解析器</summary>
        ScriptParser Parser { get; }

        /// <summary>导入脚本的结果</summary>
        /// <remarks>当返回值不为null时，则不会再调用<see cref="Parser"/>去执行</remarks>
        IScriptObject Result { get; }
    }

    class ImportInfo
    {
        private ScriptFieldInfo[][] fieldInfos;
        private IImportSource source;

        public ImportInfo(IImportSource source)
        {
            this.source = source;
        }

        public void InitFieldInfos(int contextCount)
        {
            fieldInfos = new ScriptFieldInfo[contextCount][];
        }

        public IImportSource Source
        {
            get { return source; }
        }

        public IScriptObject Result { get; set; }

        public ScriptFieldInfo GetFieldInfo(ScriptContext context, ScriptExecuteContext executeContext, int variableIndex, bool isFuncContext, string fieldName)
        {
            int contextIndex = executeContext.ContextIndex;
            ScriptFieldInfo[] list = this.fieldInfos[contextIndex];
            if (list == null) this.fieldInfos[contextIndex] = list = new ScriptFieldInfo[executeContext.VariableCount];
            ScriptFieldInfo result = list[variableIndex];
            if (result == null) list[variableIndex] = result = new ScriptFieldInfo(context, isFuncContext, fieldName);
            return result;
        }
    }

    /// <summary>脚本执行环境的上下文</summary>
    public class ScriptContext
    {
        private static ScriptObjectCreator objectCreator, arrayCreator, stringCreator, numberCreator, dateCreator;
        [ThreadStatic]
        private static ScriptContext current;
        private static ScriptMappingList defaultSystemMappings;
        private ScriptExecuteContext context;
        private Dictionary<string, object> values;
        private long objectId = ScriptHelper.ExecuteStartObjectId;
        private int contextCount;
        private ScriptFieldInfo[][] fieldInfos;
        private Dictionary<Type, ScriptTypeMembers> dicMembers;
        private ObjectMemberFlags defaultMemberFlags;
        private ScriptMappingList mappings;
        private ScriptMappingList systemMappings;
        private JumpNode jumpPath;
        private ElementBase jumpGotoPointer;
        private int invokeFlag;
        private Dictionary<string, object> cacheValues;
        private IScriptObject scriptThisObject;
        private object thisObject;
        private OperatorExecuteHandler[] optExecutes;
        private ScriptExecuteContext currentContext;
        private int execContextCounter;
        private ScriptExecuteException error;
        internal Thread executingThread;
        private bool isPaused;
        private ScriptExecuteContext pausedRootContext;
        private IImportSourceManager importManager;
        private Dictionary<string, ImportInfo> importInfos;

        #region 构造函数

        static ScriptContext()
        {
            objectCreator = new ScriptObjectCreator(new CreateScriptObjectHandler(CreateObject));
            arrayCreator = new ScriptObjectCreator(new CreateScriptObjectHandler(CreateArray));
            stringCreator = new ScriptObjectCreator(new CreateScriptObjectHandler(CreateString));
            numberCreator = new ScriptObjectCreator(new CreateScriptObjectHandler(CreateNumber));
            dateCreator = new ScriptObjectCreator(new CreateScriptObjectHandler(CreateDate));
        }

        /// <summary>默认的构造函数，使用默认的成员列表</summary>
        public ScriptContext() : this(ObjectMemberFlags.None) { }

        public ScriptContext(ObjectMemberFlags defaultMemberFlags)
        {
            this.defaultMemberFlags = defaultMemberFlags;
            this.systemMappings = defaultSystemMappings;
            if (this.systemMappings == null)
            {
                ScriptMappingList mappings = CreateSystemMappings(true);
                Interlocked.CompareExchange<ScriptMappingList>(ref defaultSystemMappings, mappings, null);
                this.systemMappings = defaultSystemMappings;
            }
        }

        /// <summary>构造函数，传入指定的成员列表</summary>
        public ScriptContext(ScriptMappingList systemMappings) : this(ObjectMemberFlags.None, systemMappings) { }

        public ScriptContext(ObjectMemberFlags defaultMemberFlags, ScriptMappingList systemMappings)
        {
            this.defaultMemberFlags = defaultMemberFlags;
            this.systemMappings = systemMappings;
        }

        #endregion

        #region 系统函数

        private static IScriptObject CreateObject(ScriptContext context)
        {
            return new ScriptType(context, typeof(ScriptObject));
        }

        private static IScriptObject CreateArray(ScriptContext context)
        {
            return new ScriptType(context, typeof(ScriptArray));
        }

        private static IScriptObject CreateString(ScriptContext context)
        {
            return new ScriptType(context, typeof(ScriptString));
        }

        private static IScriptObject CreateNumber(ScriptContext context)
        {
            return new ScriptType(context, typeof(ScriptNumber));
        }

        private static IScriptObject CreateDate(ScriptContext context)
        {
            return new ScriptType(context, typeof(ScriptDate));
        }

        [ScriptMapping("parseInt")]
        private static IScriptObject ParseInt(IScriptObject value, ScriptContext context)
        {
            if (ScriptUtils.IsNull(value)) return ScriptNumber.Create(0);
            if (value is ScriptInteger) return value;
            if (value is ScriptDecimal) return ScriptInteger.Create(((ScriptDecimal)value).IntegerValue);
            string strValue = value == null ? null : value.ToValueString(context);
            if (strValue == null) return ScriptNumber.Create(0);
            long result;
            if (long.TryParse(strValue, out result)) return ScriptNumber.Create(result);
            decimal dec;
            if (decimal.TryParse(strValue, out dec)) return ScriptNumber.Create((long)dec);
            return ScriptNumber.NaN;
        }

        [ScriptMapping("parseFloat")]
        private static IScriptObject ParseFloat(IScriptObject value, ScriptContext context)
        {
            if (value is ScriptDecimal) return value;
            if (value is ScriptInteger) return ScriptDecimal.Create(((ScriptInteger)value).DecimalValue);
            string strValue = value == null ? null : value.ToValueString(context);
            if (strValue == null) return ScriptNumber.Create(0.0M);
            decimal result;
            if (decimal.TryParse(strValue, out result)) return ScriptNumber.Create(result);
            return ScriptNumber.NaN;
        }

        [ScriptMapping("isNaN")]
        private static IScriptObject IsNaN(IScriptObject value, ScriptContext context)
        {
            return value == ScriptNumber.NaN ? ScriptBoolean.True : ScriptBoolean.False;
        }

        private static void AppendString(StringBuilder sb, IScriptObject value, ScriptContext context)
        {
            if (value is ScriptString) sb.Append('"' + ((ScriptString)value).Value + '"');
            else if (value is ScriptUndefined || value is ScriptNull || value is ScriptBoolean || value is ScriptNumber)
                sb.Append(value.ToValueString(context));
            else
            {
                IScriptArray array = value as IScriptArray;
                if (array != null && array.IsArray)
                {
                    int arrayLength = array.ArrayLength;
                    sb.Append('[');
                    for(int i = 0; i < arrayLength; i++)
                    {
                        if (i > 0) sb.Append(',');
                        AppendString(sb, array.GetElementValue(context, i), context);
                    }
                    sb.Append(']');
                }
                else
                {
                    sb.Append('{');
                    bool isFirst = true;
                    IEnumerator en = value.GetEnumerator(context, true);
                    while(en.MoveNext())
                    {
                        object obj = en.Current;
                        string key;
                        if (obj is IScriptObject)
                            key = ScriptUtils.ToValue(context, (IScriptObject)obj, typeof(string)) as string;
                        else
                            key = obj != null ? obj.ToString() : null;

                        if (key != null)
                        {
                            if (isFirst) isFirst = false;
                            else sb.Append(',');
                            sb.Append(key);
                            sb.Append(':');
                            IScriptObject v = value.GetValue(context, key);
                            AppendString(sb, v, context);
                        }
                    }
                    sb.Append('}');
                }
            }
        }

        [ScriptMapping("stringify")]
        private static IScriptObject Stringify(IScriptObject value, ScriptContext context)
        {
            string str = ScriptUtils.Stringify(context, value);
            return ScriptUtils.ToScriptObject(context, str);
        }

        [ScriptMapping("eval")]
        private static IScriptObject Eval(IScriptObject value, ScriptMethodArgus argus)
        {
            ScriptContext context = argus.Context;
            string str = value.ToValueString(context);
            if (str != null)
            {
                ElementBase currentElem = context.CurrentContext.Current;
                int flag = 0;
                if (currentElem.ResultVisit == ResultVisitFlag.Get) flag |= 1;
                ScriptObjectBase blockContext;
                DefineEvalContext evalContext = argus.BlockContextContainer as DefineEvalContext;
                if (evalContext != null)
                {
                    if (argus.BlockContextIndex >= 0)
                        blockContext = evalContext.GetBlockContext(argus.BlockContextIndex);
                    else
                        blockContext = evalContext;
                }
                else
                {
                    ScriptExecuteContext execContext = context.CurrentContext;
                    if (argus.BlockContextIndex >= 0)
                        blockContext = execContext.GetBlockContext(argus.BlockContextIndex);
                    else
                        blockContext = execContext;
                }
                ScriptParser parser = ScriptParser.ParseForEval(context, blockContext, str, flag);
                DefineContext ctx = parser.Context;
                context.SetEvalContext(ctx.First, ctx.Last);
            }
            return null;
        }

        #endregion

        #region 内部控制函数

        internal ScriptExecuteException Error
        {
            get { return error; }
            set { error = value; }
        }

        private bool InnerMoveCatch()
        {
            bool result = false;
            if (CurrentContext != null)
            {
                do
                {
                    TryCatchBlock block = CurrentContext.PeekTryBlock();
                    if (block == null)
                    {
                        if (CurrentContext == context) break;
                        PopContext();
                    }
                    else
                    {
                        result = block.CheckMoveNext(this, true);
                        if (result) break;
                        CurrentContext.PopTryBlock();
                    }
                } while (true);
            }
            return result;
        }

        internal bool CheckMoveCatch(ScriptExecuteException ex)
        {
            if (jumpPath != null)
            {
                jumpGotoPointer = null;
                jumpPath = null;
            }
            error = ex;
            return InnerMoveCatch();
        }

        private JumpNode ProcessJumpNode(JumpNode node)
        {
            ScriptExecuteContext currentContext = CurrentContext;
            while (node != null)
            {
                if (node.Type == JumpNode.TYPE_TryFinally)
                {
                    TryCatchBlock block = currentContext.PeekTryBlock();
                    if (block.CheckMoveNext(this, false)) break;
                    currentContext.PopTryBlock();
                }
                else if (node.Type == JumpNode.TYPE_Switch)
                    currentContext.PopVariable();
                node = node.Parent;
            }
            return node;
        }

        internal void Jump(JumpNode node, ElementBase gotoPointer)
        {
            if (node != null)
            {
                if (error != null) error = null;
                node = ProcessJumpNode(node);
            }
            if (node != null)
            {
                jumpPath = node;
                jumpGotoPointer = gotoPointer;
            }
            else
                MoveTo(gotoPointer);
        }

        internal void DoTryEnd()
        {
            if (jumpPath != null)
            {
                jumpPath = ProcessJumpNode(jumpPath.Parent);
                if (jumpPath == null)
                {
                    MoveTo(jumpGotoPointer);
                    jumpGotoPointer = null;
                }
            }
            else
            {
                if (error != null && !InnerMoveCatch())
                    throw error;
                MoveNext();
            }
        }

        internal void MoveNext()
        {
            if (CurrentContext != null)
                CurrentContext.Current = CurrentContext.Current.Next;
        }

        internal void MoveTo(ElementBase elem)
        {
            if (CurrentContext != null)
                CurrentContext.Current = elem;
        }

        internal void Init(int contextCount, RootExecuteContext execContext)
        {
            this.contextCount = contextCount;
            this.currentContext = this.context = execContext;
            this.execContextCounter = 0;
            if (mappings != null) execContext.ResetValueMembers(mappings);
            if (systemMappings != null) execContext.ResetSystemMembers(systemMappings);
            if (values != null)
                foreach (KeyValuePair<string, object> kv in values)
                    this.context.SetValue(null, kv.Key, ScriptUtils.ToScriptObject(this, kv.Value));
            if (this.contextCount > 0)
                this.fieldInfos = new ScriptFieldInfo[this.contextCount][];
        }

        internal void Finish()
        {
            this.error = null;
            this.jumpPath = null;
            this.jumpGotoPointer = null;
            this.cacheValues = null;
        }

        internal ScriptExecuteContext ResetRootContext(ScriptExecuteContext context)
        {
            ScriptExecuteContext result = this.context;
            this.context = context;
            return result;
        }

        internal ScriptExecuteContext CurrentContext { get { return currentContext; } }

        internal ScriptExecuteContext RootContext { get { return context; } }

        internal void PushContext(ScriptExecuteContext context)
        {
            if (++this.execContextCounter > 5000)
            {
                ElementBase elem = this.currentContext.Current;
                throw new ScriptExecuteException(elem.Parser, elem.CharIndex, "语法执行过程中出现堆栈溢出！", null);
            }
            context.PreviousContext = this.currentContext;
            this.currentContext = context;
        }

        internal ScriptExecuteContext PopContext()
        {
            ScriptExecuteContext result = this.currentContext;
            this.currentContext = (ScriptExecuteContext)result.PreviousContext;
            result.PreviousContext = null;
            this.execContextCounter--;
            return result;
        }

        internal long NewObjectId() { return ++objectId; }

        internal ScriptFieldInfo GetFieldInfo(int varIndex, bool isFuncContext, string fieldName)
        {
            ScriptExecuteContext execContext = this.CurrentContext;
            ImportInfo info = execContext.ImportInfo;
            if (info != null)
                return info.GetFieldInfo(this, execContext, varIndex, isFuncContext, fieldName);

            int contextIndex = execContext.ContextIndex;
            ScriptFieldInfo[] list = this.fieldInfos[contextIndex];
            if (list == null) this.fieldInfos[contextIndex] = list = new ScriptFieldInfo[execContext.VariableCount];
            ScriptFieldInfo result = list[varIndex];
            if (result == null) list[varIndex] = result = new ScriptFieldInfo(this, isFuncContext, fieldName);
            return result;
        }

        internal void BeginInvokeEnabled()
        {
            invokeFlag = 0;
        }

        internal void SetInvokeContext(ScriptExecuteContext execContext)
        {
            ScriptExecuteContext prevContext = this.CurrentContext;
            execContext.ResultVisit = prevContext.Current.ResultVisit;
            prevContext.Current = prevContext.Current.Next;
            PushContext(execContext);
            this.invokeFlag = 1;
        }

        internal void SetEvalContext(ElementBase first, ElementBase last)
        {
            last.Next = CurrentContext.Current.Next;
            CurrentContext.Current = first;
            this.invokeFlag = 2;
        }

        internal int EndInvokeEnabled()
        {
            int r = invokeFlag;
            invokeFlag = 0;
            return r;
        }

        private ScriptTypeMembers InternalGetMembers(Type type)
        {
            ScriptTypeMembers result;
            if (dicMembers == null)
            {
                dicMembers = new Dictionary<Type, ScriptTypeMembers>();
                result = null;
            }
            else dicMembers.TryGetValue(type, out result);
            if(result == null)
            {
                ScriptTypeMembers typeMembers = ScriptUtils.InternalGetMembers(type);
                if (typeMembers != null)
                    result = new ScriptTypeMembers(typeMembers.Type, typeMembers.ProxyType, new ScriptMemberListProxy(typeMembers.TypeMembers), new ScriptMemberListProxy(typeMembers.InstanceMembers));
                else
                    result = ScriptTypeMembers.Load(type, defaultMemberFlags);
                dicMembers[type] = result;
            }
            return result;
        }

        internal IScriptObject GetCurrentInstance()
        {
            ScriptExecuteContext p = CurrentContext;
            IScriptObject result = null;
            do
            {
                result = p.ThisObject;
                if (result != null) break;
                p = p.Parent as ScriptExecuteContext;
            } while (p != null);
            if (result == null)
                result = this.ScriptThisObject;
            return result;
        }

        internal IScriptObject Eval(string script)
        {
            if (!string.IsNullOrEmpty(script))
            {
                ScriptParser parser = ScriptParser.ParseForEval(this, null, script, 3);
                ScriptExecuteContext current = CurrentContext;
                if (current == null)
                {
                    parser.Execute(this);
                    current = CurrentContext;
                }
                else
                {
                    ElementBase currentElem = current.Current;
                    try
                    {
                        current.Current = parser.Context.First;
                        ScriptParser.InnerExecute(this, current);
                    }
                    finally
                    {
                        current.Current = currentElem;
                    }
                }
                return current.Result;
            }
            return null;
        }

        internal bool CheckPaused(ScriptExecuteContext rootContext)
        {
            if (this.isPaused)
                this.pausedRootContext = rootContext;
            return this.isPaused;
        }

        internal void CancelPaused() { this.isPaused = false; }

        internal ImportInfo GetImportInfo(IImportSource source)
        {
            if (importInfos == null)
                importInfos = new Dictionary<string, ImportInfo>(StringComparer.OrdinalIgnoreCase);
            string pathKey = source.PathKey;
            ImportInfo result;
            if (!importInfos.TryGetValue(pathKey, out result))
                importInfos[pathKey] = result = new ImportInfo(source);
            return result;
        }

        #endregion

        #region 公共方法/函数

        private void InnerRegisterType(Type type, ObjectMemberFlags memberFlags)
        {
            ScriptTypeMembers members = ScriptTypeMembers.Load(type, memberFlags);
            if (dicMembers == null) dicMembers = new Dictionary<Type, ScriptTypeMembers>();
            dicMembers[members.Type] = members;

            object[] objAttrs = type.GetCustomAttributes(typeof(ScriptMappingAttribute), true);
            if (objAttrs != null && objAttrs.Length > 0)
            {
                ScriptMappingAttribute attr = (ScriptMappingAttribute)objAttrs[0];
                string name = attr.Name;
                if (name == null) name = type.Name;
                Mappings.Register(name, new ScriptType(this, type));
            }
        }

        public ObjectMemberFlags DefaultMemberFlags { get { return defaultMemberFlags; } }

        /// <summary>注册类型</summary>
        public void RegisterType(Type type)
        {
            RegisterType(type, defaultMemberFlags);
        }

        /// <summary>注册类型</summary>
        public void RegisterType(Type type, ObjectMemberFlags memberFlags)
        {
            InnerRegisterType(type, memberFlags);
        }

        /// <summary>注册类型</summary>
        public void RegisterType(Type type, IScriptMemberList typeMembers, IScriptMemberList instanceMembers)
        {
            ScriptTypeMembers members = new ScriptTypeMembers(type, null, typeMembers, instanceMembers);
            if (dicMembers == null) dicMembers = new Dictionary<Type, ScriptTypeMembers>();
            dicMembers[type] = members;
        }

        /// <summary>注册类型</summary>
        public void RegisterTypes(ObjectMemberFlags memberFlags, params Type[] types)
        {
            if (types != null && types.Length > 0)
            {
                foreach(Type t in types)
                    InnerRegisterType(t, memberFlags);
            }
        }

        /// <summary>注册类型</summary>
        public void RegisterTypes(params Type[] types)
        {
            RegisterTypes(defaultMemberFlags, types);
        }

        /// <summary>获取类型的成员列表</summary>
        public IScriptMemberList GetTypeMembers(Type type)
        {
            ScriptTypeMembers ms = InternalGetMembers(type);
            return ms.TypeMembers;
        }

        /// <summary>获取实例的成员列表</summary>
        public IScriptMemberList GetInstanceMembers(Type type)
        {
            ScriptTypeMembers ms = InternalGetMembers(type);
            return ms.InstanceMembers;
        }

        /// <summary>注册操作符处理函数</summary>
        /// <param name="type">操作符</param>
        /// <param name="handler">处理函数</param>
        public void RegisterOperatorExecutor(OperatorType type, OperatorExecuteHandler handler)
        {
            if (optExecutes == null)
                optExecutes = new OperatorExecuteHandler[(int)OperatorType.InvokeMethod + 1];
            optExecutes[(int)type] = handler;
        }

        /// <summary>获取操作符处理函数</summary>
        public OperatorExecuteHandler GetOperatorExecutor(OperatorType type)
        {
            if (optExecutes != null) return optExecutes[(int)type];
            return null;
        }

        /// <summary>当前的脚本上下文</summary>
        public static ScriptContext Current
        {
            get { return current; }
            internal set { current = value; }
        }

        /// <summary>添加变量（只有在脚本执行之前才有效）</summary>
        /// <param name="name">变量名</param>
        /// <param name="value">变量值</param>
        public void AddValue(string name, object value)
        {
            if (values == null) values = new Dictionary<string, object>();
            values.Add(name, value);
        }

        /// <summary>设置变量（只有在脚本执行之前才有效）</summary>
        /// <param name="name">变量名</param>
        /// <param name="value">变量值</param>
        public void SetValue(string name, object value)
        {
            if (values == null) values = new Dictionary<string, object>();
            values[name] = value;
        }

        /// <summary>删除变量（只有在脚本执行之前才有效）</summary>
        /// <param name="name">变量名</param>
        public void RemoveValue(string name)
        {
            if (values != null)
                values.Remove(name);
        }

        /// <summary>获取变量的值</summary>
        /// <param name="name">变量名</param>
        /// <returns>返回变量值</returns>
        public object GetValue(string name)
        {
            object result;
            if (values != null && values.TryGetValue(name, out result)) return result;
            return null;
        }

        /// <summary>通过对类型/实例成员(标注了<c>ObjectMemberAttribute</c>的成员)的遍历，添加到脚本上下文的变量列表</summary>
        public ScriptMappingList Mappings
        {
            get
            {
                if (mappings == null)
                    mappings = new ScriptMappingList();
                return mappings;
            }
        }

        /// <summary>遍历类型的静态成员(标注了<c>ObjectMemberAttribute</c>的成员)，并将其添加到变量列表</summary>
        /// <param name="type">类型</param>
        public void AddMappings(Type type)
        {
            Mappings.AddMappings(type);
        }

        /// <summary>遍历对象的实例成员(标注了<c>ObjectMemberAttribute</c>的成员)，并将其添加到变量列表</summary>
        /// <param name="instance"></param>
        public void AddMappings(object instance)
        {
            Mappings.AddMappings(instance);
        }

        /// <summary>获取执行结果</summary>
        public IScriptObject ScriptResult { get { return context != null ? context.Result : null; } }

        /// <summary>获取脚本的“this”对象</summary>
        public IScriptObject ScriptThisObject
        {
            get
            {
                if (scriptThisObject == null)
                {
                    if (thisObject != null) scriptThisObject = ScriptUtils.ToScriptObject(this, thisObject);
                    else scriptThisObject = new ScriptObject();
                }
                return scriptThisObject;
            }
        }

        /// <summary>获取执行结果</summary>
        public object Result
        {
            get { return context != null && context.Result != null ? context.Result.ToValue(this) : null; }
        }

        /// <summary>获取或设置脚本的“this”对象</summary>
        public object ThisObject
        {
            get
            {
                if (this.thisObject == null && this.scriptThisObject != null)
                    this.thisObject = ScriptUtils.ToValue(this, this.scriptThisObject, typeof(object));
                return this.thisObject;
            }
            set
            {
                this.thisObject = value;
                this.scriptThisObject = null;
            }
        }

        /// <summary>获取缓存到脚本上下文的对象（主要是为了方便在各个.net的方法之间传递数据，与脚本本身无关）</summary>
        public object GetCacheValue(string key)
        {
            object result;
            if (cacheValues != null && cacheValues.TryGetValue(key, out result))
                return result;
            return null;
        }

        /// <summary>缓存对象到脚本上下文（主要是为了方便在各个.net的方法之间传递数据，与脚本本身无关）</summary>
        public void SetCacheValue(string key, object value)
        {
            if (key == null) throw new ArgumentNullException("key");
            if (cacheValues == null) cacheValues = new Dictionary<string, object>();
            cacheValues[key] = value;
        }

        public bool IsPaused { get { return isPaused; } }

        /// <summary>暂停执行</summary>
        public void Pause()
        {
            this.isPaused = true;
        }

        /// <summary>继续执行</summary>
        public void Continue()
        {
            if (this.pausedRootContext != null)
            {
                ScriptExecuteContext rootContext = this.pausedRootContext;
                this.isPaused = false;
                this.pausedRootContext = null;
                ScriptParser.Continue(this, rootContext);
            }
        }

        public IImportSourceManager ImportManager { get { return importManager; } set { importManager = value; } }

        /// <summary>创建一个成员列表，用于构造<c>ScriptContext</c></summary>
        /// <param name="useDefaultMappings">是否添加默认的成员</param>
        /// <returns>返回一个成员列表</returns>
        public static ScriptMappingList CreateSystemMappings(bool useDefaultMappings)
        {
            ScriptMappingList mapping = new ScriptMappingList();
            if (useDefaultMappings)
            {
                mapping.Register("Object", objectCreator);
                mapping.Register("Array", arrayCreator);
                mapping.Register("String", stringCreator);
                mapping.Register("Number", numberCreator);
                mapping.Register("Date", dateCreator);
                mapping.AddMappings(typeof(ScriptContext));
            }
            return mapping;
        }

        #endregion
    }

    /// <summary>操作符重载参数</summary>
    public class OperatorExecuteArgs
    {
        private OperatorType type;
        private IScriptObject arg1;
        private IScriptObject arg2;
        private bool isCancelled;

        internal OperatorExecuteArgs(OperatorType type, IScriptObject arg1, IScriptObject arg2)
        {
            this.type = type;
            this.arg1 = arg1;
            this.arg2 = arg2;
        }

        /// <summary>操作符</summary>
        public OperatorType Type { get { return type; } }

        /// <summary>参数1</summary>
        public IScriptObject ArgValue1 { get { return arg1; } }

        /// <summary>参数2</summary>
        public IScriptObject ArgValue2 { get { return arg2; } }

        /// <summary>取消本次重载，使用原来的操作逻辑</summary>
        public void Cancel() { this.isCancelled = true; }

        /// <summary>本次重载是否被取消</summary>
        public bool IsCancelled { get { return isCancelled; } }
    }

    /// <summary>操作符重载委托</summary>
    /// <param name="context">脚本上下文</param>
    /// <param name="args">参数</param>
    /// <returns>返回操作符执行结果</returns>
    public delegate IScriptObject OperatorExecuteHandler(ScriptContext context, OperatorExecuteArgs args);

    internal class TryCatchBlock
    {
        public const int STEP_Try = 0, STEP_Catch = 1, STEP_Finally = 2, STEP_End = 3;
        private TryStartElement tryElement;
        private int varIndex;
        private int step;
        private IScriptObject catchVariable;

        public TryCatchBlock(TryStartElement tryElement, int varIndex)
        {
            this.tryElement = tryElement;
            this.varIndex = varIndex;
        }

        public TryStartElement TryElement
        {
            get { return tryElement; }
        }

        public void SetStep(int step)
        {
            this.step = step;
        }

        public IScriptObject CatchVariable
        {
            get { return catchVariable; }
            set { catchVariable = value; }
        }

        public bool CheckMoveNext(ScriptContext context, bool checkCatch)
        {
            if (step != STEP_End) context.CurrentContext.ResetVariable(varIndex);
            switch (step)
            {
                case STEP_Try:
                    if (checkCatch && tryElement.CatchPointer != null)
                    {
                        context.MoveTo(tryElement.CatchPointer);
                        return true;
                    }
                    if (tryElement.FinallyPointer != null)
                    {
                        context.MoveTo(tryElement.FinallyPointer);
                        return true;
                    }
                    break;
                case STEP_Catch:
                    if (tryElement.FinallyPointer != null)
                    {
                        context.MoveTo(tryElement.FinallyPointer);
                        return true;
                    }
                    break;
            }
            return false;
        }
    }

    class RootExecuteContext : ScriptExecuteContext
    {
        private ScriptContext context;
        private ScriptMappingList systemMembers;
        private IScriptMemberList valueMembers;

        public RootExecuteContext(ScriptContext context) { this.context = context; }

        internal void ResetSystemMembers(ScriptMappingList systemMembers)
        {
            this.InitSystemMembers(systemMembers);
            this.systemMembers = systemMembers;
        }

        internal void ResetValueMembers(IScriptMemberList members)
        {
            this.InitValueMembers(members);
            this.valueMembers = members;
        }

        protected override int OnFindSystemMember(ScriptContext context, string key)
        {
            int index = base.OnFindSystemMember(context, key);
            if (index >= 0)
            {
                ScriptObjectCreator creator = systemMembers.InternalGetValue(context, index) as ScriptObjectCreator;
                if (creator != null)
                {
                    index = BaseSetValue(context, key, creator.CreateInstance(context));
                    if (index < 0) index = ~index;
                    index += systemMembers.Count;
                    if (valueMembers != null) index += valueMembers.Count;
                }
            }
            return index;
        }

        public override IScriptObject ThisObject
        {
            get
            {
                IScriptObject result = base.ThisObject;
                if (result == null) base.ThisObject = result = new ScriptObject();
                return result;
            }
            set
            {
                base.ThisObject = value;
            }
        }
    }

    public interface IBlockContextContainer
    {
        ScriptObjectBase GetBlockContext(int contextIndex);
    }

    class ScriptBlockContext : ScriptObjectBase
    {
        public ScriptBlockContext() : base(false)
        {
            this.ResetObjectId(ScriptHelper.DefineContextObjectId, 3);
        }

        internal override bool IsFuncContext { get { return true; } }

        public override object ToValue(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        public override string ToValueString(ScriptContext context)
        {
            throw new NotImplementedException();
        }
    }

    internal delegate void AfterFunctionExecutedHandler(ScriptContext context, ScriptExecuteContext currentContext);

    internal class ScriptExecuteContext : ScriptObjectBase, IBlockContextContainer
    {
        private readonly static CheckHashValue<IScriptObject> changeBlockValue = new CheckHashValue<IScriptObject>(CheckBlockValue);
        private ScriptExecuteContext previousContext;
        private int contextIndex;
        private int variableCount;
        private ElementBase current;
        private ResultVisitFlag resultVisit;
        private bool isNewObject;
        private IScriptObject[] varStack;
        private int varStackCount;
        private TryCatchBlock[] tryStack;
        private int tryStackCount;
        private IScriptObject thisObject, result;
        private ScriptBlockContext[] blockContexts;
        private ImportInfo importInfo;
        private AfterFunctionExecutedHandler afterFunctionExecuted;

        public ScriptExecuteContext() : base(false) { }

        internal ScriptExecuteContext PreviousContext
        {
            get { return previousContext; }
            set { previousContext = value; }
        }

        internal int ContextIndex
        {
            get { return contextIndex; }
            set { contextIndex = value; }
        }
        internal int VariableCount
        {
            get { return variableCount; }
            set { variableCount = value; }
        }
        internal ElementBase Current
        {
            get { return current; }
            set { current = value; }
        }
        internal ResultVisitFlag ResultVisit
        {
            get { return resultVisit; }
            set { resultVisit = value; }
        }
        internal bool IsNewObject
        {
            get { return isNewObject; }
            set { isNewObject = value; }
        }
        internal ImportInfo ImportInfo
        {
            get { return importInfo; }
            set { importInfo = value; }
        }

        internal void SetAfterFunctionExecuted(AfterFunctionExecutedHandler handler)
        {
            this.afterFunctionExecuted = handler;
        }

        internal void DoAfterFunctionExecuted(ScriptContext context)
        {
            if (afterFunctionExecuted != null)
                afterFunctionExecuted(context, this);
        }

        internal void SetExportVariable(ScriptContext context, string field, IScriptObject value)
        {
            if (importInfo != null)
            {
                IScriptObject obj = importInfo.Result;
                if (obj == null)
                    importInfo.Result = obj = new ScriptObject();
                obj.SetValue(context, field, value);
            }
        }

        public void PushVariable(IScriptObject obj)
        {
            if (obj == null) obj = ScriptUndefined.Value;
            if (varStack == null || varStack.Length == varStackCount)
                Array.Resize<IScriptObject>(ref varStack, varStackCount + 16);
            varStack[varStackCount++] = obj;
        }

        public IScriptObject PopVariable()
        {
            if (varStackCount > 0)
            {
                IScriptObject result = varStack[--varStackCount];
                varStack[varStackCount] = null;
                return result;
            }
            return null;
        }

        public IScriptObject PeekVariable()
        {
            if (varStackCount > 0) return varStack[varStackCount - 1];
            return null;
        }

        internal void ResetVariable(int varIndex)
        {
            if (varIndex < varStackCount)
            {
                for (int i = varIndex; i < varStackCount; i++)
                    varStack[i] = null;
                varStackCount = varIndex;
            }
        }

        private static IScriptObject CheckBlockValue(string key, IScriptObject value, object state)
        {
            DefineBlockVariableProperty p = value as DefineBlockVariableProperty;
            if (p != null)
                return new ScriptBlockVariableProperty(key, p.IsConstVariable);
            return value;
        }

        internal void ResetBlockContext(DefineBlockContext[] contexts, int contextCount)
        {
            if (contextCount > 0)
            {
                ScriptBlockContext[] bc = new ScriptBlockContext[contextCount];
                for(int i = 0; i < contextCount; i++)
                {
                    ScriptBlockContext c = new ScriptBlockContext();
                    contexts[i].CleanAssignTo(c, changeBlockValue, null, null, 0);
                    bc[i] = c;
                }
                for(int i = 0; i < contextCount; i++)
                {
                    DefineBlockContext d = contexts[i];
                    if (d.ParentIndex >= 0)
                        bc[i].Parent = bc[d.ParentIndex];
                    else
                        bc[i].Parent = this;
                }
                this.blockContexts = bc;
            }
        }

        public ScriptObjectBase GetBlockContext(int index)
        {
            if (blockContexts == null || index < 0 || index >= blockContexts.Length)
                throw new ArgumentOutOfRangeException("index");
            return blockContexts[index];
        }

        public void PushTryBlock(TryStartElement elem)
        {
            if (tryStack == null || tryStack.Length == tryStackCount)
                Array.Resize<TryCatchBlock>(ref tryStack, tryStackCount + 4);
            tryStack[tryStackCount++] = new TryCatchBlock(elem, varStackCount);
        }

        public TryCatchBlock PopTryBlock()
        {
            if (tryStackCount > 0)
            {
                TryCatchBlock result = tryStack[--tryStackCount];
                tryStack[tryStackCount] = null;
                return result;
            }
            return null;
        }

        public TryCatchBlock PeekTryBlock()
        {
            return tryStackCount > 0 ? tryStack[tryStackCount - 1] : null;
        }

        public void ResetTryBlock(int step, IScriptObject catchVariable)
        {
            TryCatchBlock block = PeekTryBlock();
            if (block != null)
            {
                block.SetStep(step);
                if (step == TryCatchBlock.STEP_Catch) block.CatchVariable = catchVariable;
                else if (step == TryCatchBlock.STEP_End) PopTryBlock();
            }
        }

        public TryCatchBlock GetTryBlockByCatchElement(CatchStartElement elem)
        {
            for (int i = tryStackCount - 1; i >= 0; i--)
            {
                TryCatchBlock item = tryStack[i];
                if (item.TryElement.CatchPointer == elem) return item;
            }
            return null;
        }

        public virtual IScriptObject ThisObject
        {
            get { return thisObject; }
            set { this.thisObject = value; }
        }

        public IScriptObject Result
        {
            get { return result == null ? ScriptUndefined.Value : result; }
            set { result = value; }
        }

        internal override bool IsFuncContext { get { return true; } }

        public override object ToValue(ScriptContext context)
        {
            throw new NotImplementedException();
        }

        public override string ToValueString(ScriptContext context)
        {
            throw new NotImplementedException();
        }
    }

}
