﻿using ShenGu.Script;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptTest
{
    public class SimpleScript : DefaultExecutor
    {
        public override string ScriptContent { get { return Utils.GetTestContent("Simple.js"); } }

        public override Type[] ScriptTypes { get { return null; } }

        public override Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["status"] = "已完成";
                return result;
            }
        }
    }

    [ScriptMapping("User")]
    public class User
    {
        [ObjectMember]
        public string Name { get; set; }

        [ObjectMember]
        public int Age { get; set; }

        [ObjectMember]
        public string Check(string operate)
        {
            return string.Format("用户（用户名：{0}，年龄：{1}）正在{2}", Name, Age, operate);
        }
    }

    public class TypeMappingExecutor : DefaultExecutor
    {
        public override string ScriptContent { get { return Utils.GetTestContent("TypeMapping.js"); } }

        public override Type[] ScriptTypes { get { return new Type[] { typeof(User) }; } }

        public override Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["User"] = typeof(User);
                return result;
            }
        }
    }

    [ScriptProxy(typeof(Form))]
    public class FormProxy : IScriptProxy
    {
        private Form form;

        public object RealInstance
        {
            get { return form; }
            set { form = (Form)value; }
        }

        [ObjectMember]
        public string Title
        {
            get { return form.Text; }
            set { form.Text = value; }
        }

        [ObjectMember]
        public void Show()
        {
            form.Show();
        }
        
        [ObjectMember]
        public void ShowDialog()
        {
            form.ShowDialog();
        }
    }

    public class TypeProxyExecutor : DefaultExecutor
    {
        public override string ScriptContent { get { return Utils.GetTestContent("TypeProxy.js"); } }

        public override Type[] ScriptTypes
        {
            get
            {
                return new Type[] { typeof(FormProxy) };
            }
        }

        public override Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["Form"] = typeof(Form);
                return result;
            }
        }
    }

    public class MethodMappingExecutor : DefaultExecutor
    {
        public override string ScriptContent { get { return Utils.GetTestContent("MethodMapping.js"); } }

        [ScriptMapping]
        public DataTable QueryUsers(int type)
        {
            DataTable table = new DataTable("Users");
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("UserName", typeof(string));
            table.Columns.Add("Age", typeof(int));
            table.Rows.Add("Admin", "管理员", 25);
            table.Rows.Add("Tester", "测试人员", 24);
            return table;
        }
    }

    public class ImportExportExecutor : DefaultExecutor
    {
        private TabData[] exportDatas;

        public ImportExportExecutor()
        {
            exportDatas = new TabData[]
            {
                new TabData()
                {
                    Title = "export.js",
                    Content = GetScriptContent("export.js")
                },
                new TabData()
                {
                    Title = "export2.js",
                    Content = GetScriptContent("export2.js")
                }
            };
        }

        private static string GetScriptContent(string fileName)
        {
            string dir = "import\\" + fileName;
            return Utils.GetTestContent(dir);
        }

        public override string ScriptContent
        {
            get { return GetScriptContent("test.js"); }
        }

        public override IImportSourceManager ImportManager { get { return new ImportSourceManager(this); } }

        public override TabData[] TabInfos { get { return exportDatas; } }

        #region 内部类

        private class ImportSource : IImportSource
        {
            private string path;
            private ImportExportExecutor executor;

            public ImportSource(ImportExportExecutor executor, string path)
            {
                this.executor = executor;
                this.path = path;
            }

            public string PathKey { get { return path; } }

            public ScriptParser Parser
            {
                get
                {
                    string content = null;
                    foreach(TabData tab in executor.exportDatas)
                    {
                        if (string.Equals(tab.Title, path, StringComparison.OrdinalIgnoreCase))
                        {
                            content = tab.Content;
                            break;
                        }
                    }
                    if (content != null)
                        return ScriptParser.Parse(content);
                    return null;
                }
            }

            public IScriptObject Result { get { return null; } }
        }

        private class ImportSourceManager : IImportSourceManager
        {
            private ImportExportExecutor executor;

            public ImportSourceManager(ImportExportExecutor executor)
            {
                this.executor = executor;
            }

            public IImportSource GetSource(ScriptContext context, string path)
            {
                return new ImportSource(executor, path);
            }
        }

        #endregion
    }

    public static class Utils
    {
        private static string rootPath;

        static Utils()
        {
            rootPath = Path.Combine(Environment.CurrentDirectory, "../../html");
        }

        public static string GetBuildPath(string url)
        {
            url = url.Replace('/', '\\');
            return Path.Combine(rootPath, "build", url);
        }

        public static string GetTestPath(string url)
        {
            url = url.Replace('/', '\\');
            return Path.Combine(rootPath, "test", url);
        }

        public static string GetBuildContent(string url)
        {
            string path = GetBuildPath(url);
            using (StreamReader reader = new StreamReader(path))
                return reader.ReadToEnd();
        }

        public static string GetTestContent(string url)
        {
            string path = GetTestPath(url);
            return File.ReadAllText(path);
        }
    }
}
