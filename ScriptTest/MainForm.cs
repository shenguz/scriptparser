﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptTest
{
    public partial class MainForm : Form
    {
        private string rootPath;

        public MainForm()
        {
            InitializeComponent();
            rootPath = Path.Combine(Environment.CurrentDirectory, "../../html");
        }

        private void btnSimpleDemo_Click(object sender, EventArgs e)
        {
            IExecutor exec = new SimpleScript();
            using (ScriptTestForm form = new ScriptTestForm(exec))
                form.ShowDialog();
        }

        private void btnTypeMappingDemo_Click(object sender, EventArgs e)
        {
            IExecutor exec = new TypeMappingExecutor();
            using (ScriptTestForm form = new ScriptTestForm(exec))
                form.ShowDialog();
        }

        private void btnTypeProxyDemo_Click(object sender, EventArgs e)
        {
            IExecutor exec = new TypeProxyExecutor();
            using (ScriptTestForm form = new ScriptTestForm(exec))
                form.ShowDialog();
        }

        private void btnMethodMappingDemo_Click(object sender, EventArgs e)
        {
            IExecutor exec = new MethodMappingExecutor();
            using (ScriptTestForm form = new ScriptTestForm(exec))
                form.ShowDialog();
        }

        private void btnImportExportDemo_Click(object sender, EventArgs e)
        {
            IExecutor exec = new ImportExportExecutor();
            using (ScriptTestForm form = new ScriptTestForm(exec))
                form.ShowDialog();
        }

        private void btnTemplateDemo_Click(object sender, EventArgs e)
        {
            string template = Utils.GetBuildContent("index.html");
            using (ScriptBuildForm form = new ScriptBuildForm(template))
                form.ShowDialog();
        }
    }
}
