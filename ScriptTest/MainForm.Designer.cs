﻿namespace ScriptTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnTypeProxyDemo = new System.Windows.Forms.Button();
            this.btnMethodMappingDemo = new System.Windows.Forms.Button();
            this.btnTypeMappingDemo = new System.Windows.Forms.Button();
            this.btnSimpleDemo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTemplateDemo = new System.Windows.Forms.Button();
            this.btnImportExportDemo = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnImportExportDemo);
            this.groupBox1.Controls.Add(this.btnTypeProxyDemo);
            this.groupBox1.Controls.Add(this.btnMethodMappingDemo);
            this.groupBox1.Controls.Add(this.btnTypeMappingDemo);
            this.groupBox1.Controls.Add(this.btnSimpleDemo);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(643, 233);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "脚本执行";
            // 
            // btnTypeProxyDemo
            // 
            this.btnTypeProxyDemo.Location = new System.Drawing.Point(336, 38);
            this.btnTypeProxyDemo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTypeProxyDemo.Name = "btnTypeProxyDemo";
            this.btnTypeProxyDemo.Size = new System.Drawing.Size(123, 75);
            this.btnTypeProxyDemo.TabIndex = 3;
            this.btnTypeProxyDemo.Text = "类型代理";
            this.btnTypeProxyDemo.UseVisualStyleBackColor = true;
            this.btnTypeProxyDemo.Click += new System.EventHandler(this.btnTypeProxyDemo_Click);
            // 
            // btnMethodMappingDemo
            // 
            this.btnMethodMappingDemo.Location = new System.Drawing.Point(489, 38);
            this.btnMethodMappingDemo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMethodMappingDemo.Name = "btnMethodMappingDemo";
            this.btnMethodMappingDemo.Size = new System.Drawing.Size(123, 75);
            this.btnMethodMappingDemo.TabIndex = 2;
            this.btnMethodMappingDemo.Text = "方法/属性映射";
            this.btnMethodMappingDemo.UseVisualStyleBackColor = true;
            this.btnMethodMappingDemo.Click += new System.EventHandler(this.btnMethodMappingDemo_Click);
            // 
            // btnTypeMappingDemo
            // 
            this.btnTypeMappingDemo.Location = new System.Drawing.Point(183, 38);
            this.btnTypeMappingDemo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTypeMappingDemo.Name = "btnTypeMappingDemo";
            this.btnTypeMappingDemo.Size = new System.Drawing.Size(123, 75);
            this.btnTypeMappingDemo.TabIndex = 1;
            this.btnTypeMappingDemo.Text = "类型映射";
            this.btnTypeMappingDemo.UseVisualStyleBackColor = true;
            this.btnTypeMappingDemo.Click += new System.EventHandler(this.btnTypeMappingDemo_Click);
            // 
            // btnSimpleDemo
            // 
            this.btnSimpleDemo.Location = new System.Drawing.Point(29, 38);
            this.btnSimpleDemo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSimpleDemo.Name = "btnSimpleDemo";
            this.btnSimpleDemo.Size = new System.Drawing.Size(123, 75);
            this.btnSimpleDemo.TabIndex = 0;
            this.btnSimpleDemo.Text = "简单例子";
            this.btnSimpleDemo.UseVisualStyleBackColor = true;
            this.btnSimpleDemo.Click += new System.EventHandler(this.btnSimpleDemo_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnTemplateDemo);
            this.groupBox2.Location = new System.Drawing.Point(16, 255);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(643, 130);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "模板生成";
            // 
            // btnTemplateDemo
            // 
            this.btnTemplateDemo.Location = new System.Drawing.Point(29, 36);
            this.btnTemplateDemo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTemplateDemo.Name = "btnTemplateDemo";
            this.btnTemplateDemo.Size = new System.Drawing.Size(123, 75);
            this.btnTemplateDemo.TabIndex = 1;
            this.btnTemplateDemo.Text = "模板生成";
            this.btnTemplateDemo.UseVisualStyleBackColor = true;
            this.btnTemplateDemo.Click += new System.EventHandler(this.btnTemplateDemo_Click);
            // 
            // btnImportExportDemo
            // 
            this.btnImportExportDemo.Location = new System.Drawing.Point(29, 137);
            this.btnImportExportDemo.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportExportDemo.Name = "btnImportExportDemo";
            this.btnImportExportDemo.Size = new System.Drawing.Size(123, 75);
            this.btnImportExportDemo.TabIndex = 4;
            this.btnImportExportDemo.Text = "Import/Export";
            this.btnImportExportDemo.UseVisualStyleBackColor = true;
            this.btnImportExportDemo.Click += new System.EventHandler(this.btnImportExportDemo_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 400);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.Text = "ScriptParser Demo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSimpleDemo;
        private System.Windows.Forms.Button btnTemplateDemo;
        private System.Windows.Forms.Button btnMethodMappingDemo;
        private System.Windows.Forms.Button btnTypeMappingDemo;
        private System.Windows.Forms.Button btnTypeProxyDemo;
        private System.Windows.Forms.Button btnImportExportDemo;
    }
}